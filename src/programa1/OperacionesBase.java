package programa1;

public class OperacionesBase {
   
   
   public int sumar (int numero1,int numero2){
       int suma = numero1+numero2;
       return suma;
   }
   
   
   public int restar (int numero1,int numero2){
       int resta = numero1-numero2;
       return resta;
   }
   
   
   public int multiplicar (int numero1,int numero2){
       int multi = numero1*numero2;
       return multi;
   }
   
   public int dividir (int numero1,int numero2){
       int divi = numero1/numero2;
       return divi;
   }
}
